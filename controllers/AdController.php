<?php
namespace App\Controllers;

    class AdController extends \App\Core\Controller {

        public function show($id) {
            $adModel = new \App\Models\AdModel($this->getDatabaseConnection());
            $ad = $adModel->getById($id);
                if(!$ad){
                    header('Location: /');
                    exit;
                }
            $this->set('ad', $ad);

            $featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $features = [];

            $adFeatureModel = new \App\Models\AdFeatureModel($this->getDatabaseConnection());
            $adFeatures = $adFeatureModel->getAllByFieldName('ad_id', $id);
            
            foreach ($adFeatures as $item) {
            	$feature = $featureModel->getById($item->feature_id);
            	$feature->value = $item->value;
            	$features[] = $feature;
            }
            
            $this->set('features', $features);
            


 
 //31 klip
        $adViewModel = new \App\Models\AdViewModel($this->getDatabaseConnection());
           $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
           $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');

            $adViewModel->add(
                [
                    'ad_id' => $id,
                    'ip_address' => $ipAddress,
                    'user_agent' => $userAgent
                ]
            );
//
        }

        private function normaliseKeywords(string $keywords): string {
            $keywords = trim($keywords);
            $keywords = preg_replace('/ +/', ' ', $keywords);
            #....proverava da nije ukucano vise razmaka nego sto treba
            return $keywords;
        }


        public function postSearch(){
            $adModel = new \App\Models\AdModel($this->getDatabaseConnection());

            $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

            $keywords = $this->normaliseKeywords($q);
            $ads = $adModel->getAllBySearch($q);      

            $this->set('ads', $ads);
        }


        public function postAdvancedSearch(){
            $adModel        = new \App\Models\AdModel($this->getDatabaseConnection());
            $adFeatureModel = new \App\Models\AdFeatureModel($this->getDatabaseConnection());


            $priceStarts = filter_input(INPUT_POST, 'priceStarts', FILTER_SANITIZE_NUMBER_INT);
            $priceEnds   = filter_input(INPUT_POST, 'priceEnds',   FILTER_SANITIZE_NUMBER_INT);
            $fuel        = filter_input(INPUT_POST, 'fuel',        FILTER_SANITIZE_STRING);
            $bodytype    = filter_input(INPUT_POST, 'bodytype',    FILTER_SANITIZE_STRING);



            $ads1 = $adFeatureModel->getAllByFuelAndPrice(strval($fuel), intval($priceStarts), intval($priceEnds));  
           # print_r($ads);

            $array = array_column($ads1, 'ad_id');
          
            $ads = [];
            foreach ($array as $id => $v) {
                $ads1 = $adFeatureModel->getAllByBodyType($v, strval($bodytype));
                $empty = array_filter($ads1);
                    if(!empty($empty)){
                        $ads[] = $adModel->getById($v);
                    }
            }

            $this->set('ads', $ads);



        }


   
    }