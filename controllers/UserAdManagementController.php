<?php
namespace App\Controllers;

class UserAdManagementController extends \App\Core\Role\UserRoleController {
    public function ads(){
        $userId = $this->getSession()->get('user_id');
        $adModel    = new \App\Models\AdModel($this->getDatabaseConnection());
        $ads = $adModel->getAllByUserId($userId);
        $this->set('ads', $ads);
    }


    public function getEdit($adId){
            $adModel = new \App\Models\AdModel($this->getDatabaseConnection());
            $ad = $adModel->getById($adId);


                if(!$ad){
                    $this->redirect(\Configuration::BASE . 'user/ads');
                    return;
                }
#klip 59 onemoguciti korisniku da procita tudje aukcije kopiranjem u URL
                if($ad->user_id != $this->getSession()->get('user_id')){
                    $this->redirect(\Configuration::BASE . 'user/ads');
                    return;
                }

            $this->set('ad', $ad);

#edit modela i kategorije
        $modelModel    = new \App\Models\ModelModel($this->getDatabaseConnection());
        $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
    	$categories = $categoryModel->getAll();
    	for ($i=0; $i<count($categories); $i++) {
    		$categories[$i]->models = $modelModel->getAllByFieldName('category_id', $categories[$i]->category_id);
    	}
        $this->set('categories', $categories);

#edit feature 

        $featureModel   = new \App\Models\FeatureModel($this->getDatabaseConnection());
        $adFeatureModel = new \App\Models\AdFeatureModel($this->getDatabaseConnection());
        $adFeatures     = $adFeatureModel->getAllByFieldName('ad_id', $adId); #daj mi sve karakteristike gde je ad_id = onaj na koji se kliknulo
        #$adFeatures     = $adFeatureModel->deleteAllAdFeaturesByAdId($adId); 

        $empty = array_filter($adFeatures);
            if(!empty($empty)){
                $features = [];
                    foreach ($adFeatures as $item) {
                        $feature = $featureModel->getById($item->feature_id);
                        $feature->value = $item->value;
                        $features[] = $feature;
                    }
                
                $this->set('features', $features);
                return;
            }


                $features = $featureModel->getAll();
                for ($i=0; $i<count($features); $i++) {
                    $features[$i]->adFeatures = $adFeatureModel->getAllByFieldName('feature_id', $features[$i]->feature_id);
                }
                
                $this->set('features', $features);

        
    }
    


    public function postEdit($adId) {
        $this->getEdit($adId);

        $editData = [
            'title'       => \filter_input(INPUT_POST, 'title',       FILTER_SANITIZE_STRING),
            'description' => \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING),
            'price'       => \filter_input(INPUT_POST, 'price',       FILTER_SANITIZE_STRING),
            'model_id'    => \filter_input(INPUT_POST, 'model_id',    FILTER_SANITIZE_NUMBER_INT)
        ];

        $adModel = new \App\Models\AdModel($this->getDatabaseConnection());
        $res = $adModel->editById($adId, $editData);

            if(!$res){
                $this->set('message', 'Nije moguce izmeniti oglas.');
                return;
            }

            if(isset($_FILES['image']) && $_FILES['image']['error'] == 0 ){
                $uploadStatus = $this->doImageUpload('image', $adId);
                if(!$uploadStatus){
                    return;
                }
            }

            $featureIds  = filter_input(INPUT_POST, 'feature_ids',  FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);

            $featureModel   = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $adFeatureModel = new \App\Models\AdFeatureModel($this->getDatabaseConnection());
            $adFeatures     = $adFeatureModel->deleteAllAdFeaturesByAdId($adId); 


            foreach ($featureIds as $id => $v) {
                $adFeatureModel->add([
                    'ad_id'      => $adId,
                    'feature_id' => intval($id),
                    'value'      => strval($v)
                ]);
            }
         

        $this->redirect(\Configuration::BASE . 'user/ads');

    }




    public function getAdd(){
    	$modelModel    = new \App\Models\ModelModel($this->getDatabaseConnection());
        $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
    	$categories = $categoryModel->getAll();
    	for ($i=0; $i<count($categories); $i++) {
    		$categories[$i]->models = $modelModel->getAllByFieldName('category_id', $categories[$i]->category_id);
    	}
    	$this->set('categories', $categories);
    	
        
        

    	$featureModel = new \App\Models\FeatureModel($this->getDatabaseConnection());
        $adFeatureModel = new \App\Models\AdFeatureModel($this->getDatabaseConnection());
        $features= $featureModel->getAll();

        for ($i=0; $i<count($features); $i++) {
    		$features[$i]->adFeatures = $adFeatureModel->getAllByFieldName('feature_id', $features[$i]->feature_id);
    	}
        $this->set('features', $features);


    }

    public function postAdd(){
        $addData = [
            'title'       => \filter_input(INPUT_POST, 'title',       FILTER_SANITIZE_STRING),
            'description' => \filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING),
            'price'       => \filter_input(INPUT_POST, 'price',       FILTER_SANITIZE_STRING),
            'model_id'    => \filter_input(INPUT_POST, 'model_id',    FILTER_SANITIZE_NUMBER_INT),
            'user_id'     => $this->getSession()->get('user_id'),
        ];
       
            $featureIds  = filter_input(INPUT_POST, 'feature_ids',  FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);

            $featureModel   = new \App\Models\FeatureModel($this->getDatabaseConnection());
            $adFeatureModel = new \App\Models\AdFeatureModel($this->getDatabaseConnection());
            $adModel        = new \App\Models\AdModel($this->getDatabaseConnection());
                            #popuni ad tabelu
                            $adId = $adModel->add($addData);
                    
                                if(!$adId) {
                                    $this->set('message', 'Doslo je do greske: Nije moguce dodati ovaj oglas');
                                    return;
                                }
                
                                $uploadStatus = $this->doImageUpload('image', $adId);
                                if(!$uploadStatus){
                                    return;
                                }

                            #popuni adFeature tabelu
                            foreach ($featureIds as $id => $v) {
                                $adFeatureModel->add([
                                    'ad_id'      => $adId,
                                    'feature_id' => intval($id),
                                    'value'      => strval($v)
                                ]);
                            }
                
                            $this->redirect(\Configuration::BASE . 'user/ads');
                            $isActive = 1;
    }


    private function doImageUpload(string $fieldName, string $adId): bool{
        $adModel = new \App\Models\AdModel($this->getDatabaseConnection());
        $ad = $adModel->getById(intval($adId));
        #codeguy/upload

        unlink(\Configuration::UPLOAD_DIR . $ad->image_path);

        $uploadPath = new \Upload\Storage\FileSystem(\Configuration::UPLOAD_DIR);
        $file = new \Upload\File($fieldName, $uploadPath);
        $file->setName($adId);
        $file->addValidations([
            new \Upload\Validation\Mimetype(["image/jpeg", "image/png"]),
            new \Upload\Validation\Size("3M")
        ]);


        try{
            $file->upload();

            $fullFileName = $file->getNameWithExtension();
           
            $adModel->editById(intval($adId), [
                'image_path'=> $fullFileName
            ]);

            $this->doResize(
                \Configuration::UPLOAD_DIR . $fullFileName,
                \Configuration::DEFAULT_IMAGE_WIDTH,
                \Configuration::DEFAULT_IMAGE_HEIGHT);

                return true;
           
        } catch(\Exception $e){
            $this->set('message', 'Greska:' . implode(', ', $file->getErrors()));
            return false;
        }
    }

    private function doResize(string $filePath, int $w, int $h) {
        $longer = max($w, $h);

        $image = new \Gumlet\ImageResize($filePath);
        $image->resizeToBestFit($longer, $longer);
        $image->crop($w, $h);
        $image->save($filePath);
    }


}