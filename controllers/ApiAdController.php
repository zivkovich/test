<?php
namespace App\Controllers;

    class ApiAdController extends \App\Core\ApiController {
       public function show($id){
            $adModel = new \App\Models\AdModel($this->getDatabaseConnection());
            $ad = $adModel->getById($id);
            $this->set('ad', $ad);

       }
    }