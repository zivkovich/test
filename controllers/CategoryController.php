<?php
//NE SME DA RADI SA BAZOM PODATAKA, uzme dbc ali ga prosledjuje MODELU!!!
namespace App\Controllers;

    class CategoryController extends \App\Core\Controller {
        public function show($id) {
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $category = $categoryModel->getById($id);

            
            if(!$category){
                header('Location: /');
                exit;
            }

            $this->set('category', $category);




            $modelModel = new \App\Models\ModelModel($this->getDatabaseConnection());
            $models = $modelModel->getAllByCategoryId($id);
            $this->set('models', $models);
        }

        }