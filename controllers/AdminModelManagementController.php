<?php
namespace App\Controllers;

class AdminModelManagementController extends \App\Core\Role\UserRoleController {
    public function models(){
            $modelModel = new \App\Models\ModelModel($this->getDatabaseConnection());
            $models = $modelModel->getAll();
            $this->set('models', $models);
    }

    public function getEdit($modelId){
            $modelModel = new \App\Models\ModelModel($this->getDatabaseConnection());
            $model = $modelModel->getById($modelId);

            if(!$model){
                $this->redirect(\Configuration::BASE . 'admin/models');
            }
            
            $this->set('model', $model);

            return $modelModel;
    }

    public function postEdit($modelId) {
            $modelModel = $this->getEdit($modelId);

            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $modelModel->editById($modelId, [
                'name' => $name
            ]);

            $this->redirect(\Configuration::BASE . 'admin/models');

    }

    public function getAdd(){

    }

    public function postAdd(){
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

            $modelModel = new \App\Models\CategoryModel($this->getDatabaseConnection());

            $modelId = $modelModel->add([
                'name' => $name
            ]);

            if($modelId) {
                $this->redirect(\Configuration::BASE . 'admin/models');
            }

            $this->set('message', 'Doslo je do greske: Nije moguce dodati ovaj model!');

    }
}