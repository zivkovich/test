<?php
//NE SME DA RADI SA BAZOM PODATAKA, uzme dbc ali ga prosledjuje MODELU!!!
namespace App\Controllers;

    class ModelController extends \App\Core\Controller {
        public function show($id) {
            $modelModel = new \App\Models\ModelModel($this->getDatabaseConnection());
            $model = $modelModel->getById($id);
            //posrednik koji prihvata zahteve iz spoljasnjeg sveta i pozvace adekvatan kontroler 
            //pa metod i proslediti view sta da bude prikazano

            if(!$model){
                header('Location: /');
                exit;
            }
            
            $this->set('model', $model);

            $adModel = new \App\Models\AdModel($this->getDatabaseConnection());
            $adsInModel = $adModel->getAllByModelId($id);
            $this->set('adsInModel', $adsInModel);
        }

        public function delete($id){
            die('Nije zavrsena implementacija brisanja...');
        }
    }