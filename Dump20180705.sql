-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: oglasi
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ad`
--

DROP TABLE IF EXISTS `ad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad` (
  `ad_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` int(11) unsigned NOT NULL,
  `starts_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ends_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `user_id` int(11) unsigned NOT NULL,
  `model_id` int(11) unsigned NOT NULL,
  `image_path` varchar(255) NOT NULL,
  PRIMARY KEY (`ad_id`),
  UNIQUE KEY `uq_ad_image_path` (`image_path`) USING BTREE,
  KEY `fk_ad_user_id` (`user_id`),
  KEY `fk_ad_model_id` (`model_id`),
  CONSTRAINT `fk_ad_model_id` FOREIGN KEY (`model_id`) REFERENCES `model` (`model_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ad_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad`
--

LOCK TABLES `ad` WRITE;
/*!40000 ALTER TABLE `ad` DISABLE KEYS */;
INSERT INTO `ad` VALUES (1,'2018-05-11 10:21:10','Yaris 2003','toyota yaris 2003 godiste',3000,'2018-05-28 04:46:54','2018-06-28 04:46:54',1,6,1,'1.jpg'),(2,'2018-05-11 21:11:51','T-Sport Yaris','sportski auto u odlicnom stanju...',4000,'2018-05-20 10:46:54','2018-06-20 10:46:54',1,2,1,'2.jpg'),(3,'2018-05-11 21:11:51','Toyota Yaris Verso 1.3','Manuelni 5 brzina,  Automatska klima,  4/5 vrata, 5 sedišta',3700,'2018-05-10 08:50:54','2018-06-10 08:50:54',1,3,1,'3.jpg'),(4,'2018-05-11 21:11:51','Toyota Hilux 3.0 D4D','Toyota kupljena nova u Srbiji, sve radjeno u ovlašćenom servisu, uradjen 9.3.2018. redovan servis u ovlašćenom servisu na 139524km, bez dinara ulaganja.',19990,'2018-05-16 01:50:54','2018-06-16 01:50:54',1,4,3,'4.jpg'),(5,'2018-06-04 18:11:51','Toyota Celica 1.6','tojota odlicna ,zdrava prava ,motor extra ,mali potrosac, za vise informacije o autu i dogovoru pozovite ili sms ',1700,'2018-06-16 01:50:00','2018-07-16 01:50:00',1,5,2,'5.jpg'),(6,'2018-06-05 09:29:14','2003 Toyota Avensis Verso 2.0 D4D',' Toyota Avensis Verso 2.0 D4D',2500,'2018-06-16 01:50:54','2018-07-16 01:50:54',1,5,7,'6.jpg'),(36,'2018-07-04 08:00:43','2003 Toyota RAV4','Auto odlicnom stanju, 4 dobre gume, dobar za sneg i kidne uslove za vise informacija pozvati\r\n\r\n',4200,'2018-07-04 08:00:43','2018-08-03 08:00:43',1,5,8,'36.jpg'),(37,'2018-07-04 11:51:59','1991 Toyota Celica 2.0 GTI','Registracija istekla pre dve godine, nije objavljena.  \r\nTada je kupljen drugi auto i od tada ona stoji.  \r\nU međuvremenu je radila u dvorištu, redovno paljenje.  \r\nPotreban nov akumulator.  \r\nTruo je levi prag i malo kod zadnjeg desnog blatobrana.  \r\nSve se vidi na slikma.  \r\nŠiber funkcioniše, kao i automatsko spuštanje volana.  \r\nFarba je delimično izbledela, ali na slikama se baš i ne primećuje to.  \r\nMoje je da napomenem.  \r\n\r\nKontakt ISKLJUČIVO SMSom, ili poruka na KPu.  \r\nKome nešto ne odgovara - nek&#39; zaobiđe oglas!  \r\nNe pametujte!  ',550,'2018-07-04 11:51:59','2018-08-03 11:51:59',1,6,2,'37.jpg'),(38,'2018-07-05 08:59:37','2004 Toyota RAV4 VVT-1.5 V ','Toyota RAV4, HITNA prodaja, ja sam drugi vlasnik, prodajem ga jer idem u \r\nAmeriku da radim, auto nije dugo kod mene, oko 9 meseci. \r\nPotrebna sitna ulaganja, zadnji branik (vidi se na slikama), merač za gorivo, levi prozor zna da zaglavi. Menjan zupčanik menjača pre 5 meseci. \r\nOdličan auto, okretan, lakoupravljiv i izdržljiv. \r\nPogodan i za gradsku vožnju i za putovanje. \r\nCena je više nego korektna i snižena zbog isteka registracije u medjuvremenu, istekla je krajem aprila. CENA JE FIKSNA. Ne zanimaju me zamene. \r\nZa sve ostale informacije pozovite slobodno.',3500,'2018-07-05 08:59:37','2018-08-04 08:59:37',1,5,8,'38.jpg'),(39,'2018-07-05 09:07:52','1990 Toyota Corolla 1300XL ','- Toyota Corolla 1300XL Sedan 1990 godište, prešla 272 776 km. \r\n- Auto je dosta solidan za svoje godine, redovno održavan i servisiran! \r\n- Limarijski bez ulaganja.  Enterijer takođe. Sve četri gume dobre M+S\r\n- Rezervni ključ. \r\n- Mali servis je urađen. Poželjno je zameniti zadnje amortizere i centrirati trap. \r\n- Svaka pomoć oko papirologije. \r\n\r\nZa više informacija slobodno pozovite. Saša 063651363',680,'2018-07-05 09:07:52','2018-08-04 09:07:52',1,9,9,'39.jpg');
/*!40000 ALTER TABLE `ad` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `oglasi`.`ad_BEFORE_INSERT` BEFORE INSERT ON `ad` FOR EACH ROW
BEGIN
    SET NEW.ends_at = ADDDATE(NOW(), INTERVAL 30 DAY);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ad_feature`
--

DROP TABLE IF EXISTS `ad_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_feature` (
  `ad_feature_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ad_id` int(11) unsigned NOT NULL,
  `feature_id` int(11) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`ad_feature_id`),
  KEY `fk_ad_feature_feature_id` (`feature_id`),
  KEY `fk_ad_feature_ad_id` (`ad_id`),
  CONSTRAINT `fk_ad_feature_ad_id` FOREIGN KEY (`ad_id`) REFERENCES `ad` (`ad_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_ad_feature_feature_id` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`feature_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=352 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_feature`
--

LOCK TABLES `ad_feature` WRITE;
/*!40000 ALTER TABLE `ad_feature` DISABLE KEYS */;
INSERT INTO `ad_feature` VALUES (27,'2018-07-04 08:53:16',6,1,'dizel'),(28,'2018-07-04 08:53:16',6,2,'limuzina'),(29,'2018-07-04 08:53:16',6,3,'4'),(30,'2018-07-04 08:53:16',6,4,'siva'),(31,'2018-07-04 08:53:16',6,5,'2011'),(32,'2018-07-04 08:53:16',6,6,'1998'),(33,'2018-07-04 08:53:17',6,7,'126'),(34,'2018-07-04 08:53:17',6,8,'ne'),(35,'2018-07-04 08:53:17',6,9,'ne'),(36,'2018-07-04 08:53:17',6,10,'da'),(37,'2018-07-04 08:53:17',6,11,'da'),(38,'2018-07-04 08:53:17',6,12,'da'),(39,'2018-07-04 08:53:17',6,13,'da'),(131,'2018-07-04 10:55:49',1,1,'dizel'),(132,'2018-07-04 10:55:49',1,2,'kupe'),(133,'2018-07-04 10:55:49',1,3,'3'),(134,'2018-07-04 10:55:49',1,4,'siva'),(135,'2018-07-04 10:55:49',1,5,'2001'),(136,'2018-07-04 10:55:49',1,6,'1000'),(137,'2018-07-04 10:55:50',1,7,'106'),(138,'2018-07-04 10:55:50',1,8,'da'),(139,'2018-07-04 10:55:50',1,9,'da'),(140,'2018-07-04 10:55:50',1,10,'da'),(141,'2018-07-04 10:55:50',1,11,'da'),(142,'2018-07-04 10:55:50',1,12,'da'),(143,'2018-07-04 10:55:50',1,13,'da'),(222,'2018-07-04 15:06:19',5,1,'benzin'),(223,'2018-07-04 15:06:19',5,2,'kupe'),(224,'2018-07-04 15:06:19',5,3,'3'),(225,'2018-07-04 15:06:19',5,4,'siva'),(226,'2018-07-04 15:06:19',5,5,'1991'),(227,'2018-07-04 15:06:19',5,6,'1587'),(228,'2018-07-04 15:06:19',5,7,'106'),(229,'2018-07-04 15:06:19',5,8,'ne'),(230,'2018-07-04 15:06:19',5,9,'ne'),(231,'2018-07-04 15:06:19',5,10,'ne'),(232,'2018-07-04 15:06:19',5,11,'ne'),(233,'2018-07-04 15:06:20',5,12,'ne'),(234,'2018-07-04 15:06:20',5,13,'ne'),(235,'2018-07-05 08:59:38',38,1,'benzin'),(236,'2018-07-05 08:59:38',38,2,'dzip'),(237,'2018-07-05 08:59:38',38,3,'4'),(238,'2018-07-05 08:59:38',38,4,'plava'),(239,'2018-07-05 08:59:38',38,5,'2004'),(240,'2018-07-05 08:59:38',38,6,'1800'),(241,'2018-07-05 08:59:38',38,7,'96'),(242,'2018-07-05 08:59:38',38,8,'ne'),(243,'2018-07-05 08:59:38',38,9,'ne'),(244,'2018-07-05 08:59:38',38,10,'ne'),(245,'2018-07-05 08:59:38',38,11,'da'),(246,'2018-07-05 08:59:38',38,12,'da'),(247,'2018-07-05 08:59:38',38,13,'da'),(248,'2018-07-05 09:03:45',36,1,'dizel'),(249,'2018-07-05 09:03:45',36,2,'dzip'),(250,'2018-07-05 09:03:45',36,3,'4'),(251,'2018-07-05 09:03:46',36,4,'siva'),(252,'2018-07-05 09:03:46',36,5,'2003'),(253,'2018-07-05 09:03:46',36,6,'2000'),(254,'2018-07-05 09:03:46',36,7,'85'),(255,'2018-07-05 09:03:46',36,8,'ne'),(256,'2018-07-05 09:03:46',36,9,'ne'),(257,'2018-07-05 09:03:46',36,10,'ne'),(258,'2018-07-05 09:03:46',36,11,'ne'),(259,'2018-07-05 09:03:46',36,12,'da'),(260,'2018-07-05 09:03:46',36,13,'da'),(287,'2018-07-05 09:12:58',39,1,'benzin'),(288,'2018-07-05 09:12:58',39,2,'limuzina'),(289,'2018-07-05 09:12:58',39,3,'4'),(290,'2018-07-05 09:12:58',39,4,'siva'),(291,'2018-07-05 09:12:59',39,5,'1990'),(292,'2018-07-05 09:12:59',39,6,'3000'),(293,'2018-07-05 09:12:59',39,7,'55'),(294,'2018-07-05 09:12:59',39,8,'ne'),(295,'2018-07-05 09:12:59',39,9,'ne'),(296,'2018-07-05 09:12:59',39,10,'ne'),(297,'2018-07-05 09:12:59',39,11,'da'),(298,'2018-07-05 09:12:59',39,12,'da'),(299,'2018-07-05 09:12:59',39,13,'da'),(300,'2018-07-05 09:18:13',37,1,'benzin'),(301,'2018-07-05 09:18:13',37,2,'kupe'),(302,'2018-07-05 09:18:13',37,3,'3'),(303,'2018-07-05 09:18:13',37,4,'crvena'),(304,'2018-07-05 09:18:14',37,5,'1991'),(305,'2018-07-05 09:18:14',37,6,'2000'),(306,'2018-07-05 09:18:14',37,7,'96'),(307,'2018-07-05 09:18:14',37,8,'ne'),(308,'2018-07-05 09:18:14',37,9,'ne'),(309,'2018-07-05 09:18:14',37,10,'ne'),(310,'2018-07-05 09:18:14',37,11,'ne'),(311,'2018-07-05 09:18:14',37,12,'da'),(312,'2018-07-05 09:18:14',37,13,'da'),(313,'2018-07-05 09:24:28',4,1,'benzin'),(314,'2018-07-05 09:24:28',4,2,'dzip'),(315,'2018-07-05 09:24:28',4,3,'4'),(316,'2018-07-05 09:24:28',4,4,'crvena'),(317,'2018-07-05 09:24:29',4,5,'2010'),(318,'2018-07-05 09:24:29',4,6,'3000'),(319,'2018-07-05 09:24:29',4,7,'55'),(320,'2018-07-05 09:24:29',4,8,'da'),(321,'2018-07-05 09:24:29',4,9,'da'),(322,'2018-07-05 09:24:29',4,10,'da'),(323,'2018-07-05 09:24:29',4,11,'da'),(324,'2018-07-05 09:24:29',4,12,'da'),(325,'2018-07-05 09:24:29',4,13,'da');
/*!40000 ALTER TABLE `ad_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ad_view`
--

DROP TABLE IF EXISTS `ad_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_view` (
  `ad_view_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ad_id` int(11) unsigned NOT NULL,
  `ip_address` varchar(24) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  PRIMARY KEY (`ad_view_id`),
  KEY `fk_ad_view_ad_id` (`ad_id`),
  CONSTRAINT `fk_ad_view_ad_id` FOREIGN KEY (`ad_id`) REFERENCES `ad` (`ad_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=915 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ad_view`
--

LOCK TABLES `ad_view` WRITE;
/*!40000 ALTER TABLE `ad_view` DISABLE KEYS */;
INSERT INTO `ad_view` VALUES (1,'2018-05-11 10:36:20',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36'),(841,'2018-06-25 16:13:08',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'),(842,'2018-06-30 11:11:20',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(843,'2018-06-30 11:11:20',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(844,'2018-06-30 11:11:22',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(845,'2018-07-01 11:41:44',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(846,'2018-07-01 12:27:45',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(847,'2018-07-03 15:50:12',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(848,'2018-07-03 17:23:13',2,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(849,'2018-07-03 17:23:17',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(850,'2018-07-03 17:32:08',2,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(851,'2018-07-03 17:32:46',2,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(852,'2018-07-03 17:34:03',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(853,'2018-07-03 17:34:45',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(854,'2018-07-03 17:34:55',2,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(855,'2018-07-04 07:02:14',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(856,'2018-07-04 07:14:27',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(857,'2018-07-04 08:33:37',36,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(858,'2018-07-04 08:51:19',6,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(859,'2018-07-04 08:53:47',6,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(860,'2018-07-04 08:54:04',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(861,'2018-07-04 10:43:04',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(862,'2018-07-04 19:46:01',5,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(863,'2018-07-05 07:55:50',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(864,'2018-07-05 07:57:30',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(865,'2018-07-05 07:57:34',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(866,'2018-07-05 07:58:59',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(867,'2018-07-05 08:01:02',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(868,'2018-07-05 08:01:35',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(869,'2018-07-05 08:03:17',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(870,'2018-07-05 08:04:03',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(871,'2018-07-05 08:04:04',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(872,'2018-07-05 08:04:04',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(873,'2018-07-05 08:04:28',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(874,'2018-07-05 08:04:55',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(875,'2018-07-05 08:04:56',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(876,'2018-07-05 08:04:56',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(877,'2018-07-05 08:04:56',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(878,'2018-07-05 08:05:37',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(879,'2018-07-05 08:05:38',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(880,'2018-07-05 08:05:38',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(881,'2018-07-05 08:05:39',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(882,'2018-07-05 08:06:27',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(883,'2018-07-05 08:07:05',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(884,'2018-07-05 08:07:06',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(885,'2018-07-05 08:07:44',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(886,'2018-07-05 08:07:56',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(887,'2018-07-05 08:08:26',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(888,'2018-07-05 08:09:18',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(889,'2018-07-05 08:10:32',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(890,'2018-07-05 08:11:01',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(891,'2018-07-05 08:13:11',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(892,'2018-07-05 08:13:35',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(893,'2018-07-05 08:13:37',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(894,'2018-07-05 08:13:38',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(895,'2018-07-05 08:13:49',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(896,'2018-07-05 08:15:25',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(897,'2018-07-05 08:15:41',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(898,'2018-07-05 08:16:06',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(899,'2018-07-05 08:19:29',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(900,'2018-07-05 09:14:12',37,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(901,'2018-07-05 09:18:27',37,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(902,'2018-07-05 09:18:33',37,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(903,'2018-07-05 09:19:22',37,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(904,'2018-07-05 09:19:40',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(905,'2018-07-05 09:19:49',5,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(906,'2018-07-05 09:19:53',37,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(907,'2018-07-05 09:20:00',4,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(908,'2018-07-05 09:21:34',6,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(909,'2018-07-05 09:21:43',36,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(910,'2018-07-05 09:21:47',38,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(911,'2018-07-05 09:21:59',39,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(912,'2018-07-05 09:26:46',4,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(913,'2018-07-05 09:37:07',39,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),(914,'2018-07-05 09:38:48',1,'::1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36');
/*!40000 ALTER TABLE `ad_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `uq_category_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (9,'Audi'),(10,'BMW'),(4,'Jeep'),(5,'Kia'),(2,'Lexus'),(6,'Mazda'),(8,'Mitsubishi'),(11,'Nissan'),(12,'Renault'),(7,'Smart'),(3,'Suzuki'),(1,'Toyota');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feature`
--

DROP TABLE IF EXISTS `feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature` (
  `feature_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  `is_mandatory` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`feature_id`),
  UNIQUE KEY `uq_feature_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feature`
--

LOCK TABLES `feature` WRITE;
/*!40000 ALTER TABLE `feature` DISABLE KEYS */;
INSERT INTO `feature` VALUES (1,'2018-05-11 14:51:30','Gorivo',1),(2,'2018-05-11 14:51:30','Karoserija',1),(3,'2018-05-11 14:51:30','Broj vrata',1),(4,'2018-05-11 14:51:30','Boja',1),(5,'2018-05-11 14:51:30','Godiste',1),(6,'2018-05-11 14:51:30','Zapremina motora',1),(7,'2018-05-11 14:51:30','Snaga motora',1),(8,'2018-05-11 14:51:30','Posebna elektronska oprema',1),(9,'2018-05-11 14:51:30','Posebni kocioni sistemi',1),(10,'2018-05-11 14:51:30','Luksuzna oprema enterijera',1),(11,'2018-05-11 14:51:30','Registracija',1),(12,'2018-05-11 14:51:30','Vlasnik',1),(13,'2018-05-11 14:51:30','Dokumentacija',1);
/*!40000 ALTER TABLE `feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model`
--

DROP TABLE IF EXISTS `model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model` (
  `model_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`model_id`),
  UNIQUE KEY `uq_model_name` (`name`),
  KEY `fk_model_category_id` (`category_id`),
  CONSTRAINT `fk_model_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model`
--

LOCK TABLES `model` WRITE;
/*!40000 ALTER TABLE `model` DISABLE KEYS */;
INSERT INTO `model` VALUES (1,'Yaris',1),(2,'Celica',1),(3,'Hilux',1),(4,'Vitara',3),(5,'IS200',2),(6,'IS220',2),(7,'Avensis',1),(8,'RAV4',1),(9,'Corolla',1),(10,'Renegade',4),(11,'Compass',4),(12,'Cherokee',4),(13,'Grand Cherokee',4);
/*!40000 ALTER TABLE `model` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(128) NOT NULL,
  `forename` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uq_user_username` (`username`),
  UNIQUE KEY `uq_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2018-05-09 19:17:19','kaca','kaca@gmail.com','$2y$10$86igNPkw6PwAORk8XyW.TeNyj90W/SP4ytC5DIDzjP9D6D774t/mS','Katarina','Zivkovic',1),(2,'2018-05-09 19:17:50','zika','zika@gmail.com','$2y$10$86igNPkw6PwAORk8XyW.TeNyj90W/SP4ytC5DIDzjP9D6D774t/mS','Zika','Zikic',1),(3,'2018-05-09 19:18:28','pera','pera@gmail.com','$2y$10$86igNPkw6PwAORk8XyW.TeNyj90W/SP4ytC5DIDzjP9D6D774t/mS','Pera','Peric',1),(4,'2018-05-09 19:27:18','didi','didi@gmail.com','$2y$10$86igNPkw6PwAORk8XyW.TeNyj90W/SP4ytC5DIDzjP9D6D774t/mS','Dunja','Kovacevic',1),(5,'2018-05-11 10:24:09','cath','katarina.zivkovic.14@singimail.rs','$2y$10$86igNPkw6PwAORk8XyW.TeNyj90W/SP4ytC5DIDzjP9D6D774t/mS','Katarina','Zivkovic',1),(6,'2018-05-11 20:45:30','sofija','sofija@gmail.com','$2y$10$eKp1Ua0UYJVFXynrHY.mrOHWKVTp1yQ81v07/hGoPh6EjLZhL6PR.','Sofija','Martin',1),(7,'2018-06-06 11:08:40','admin','admin@gmail.rs','$2y$10$t3myMK70weXF/GF8yOWysOSadBGnajFsqsNfTZ4qIB7JYjMTGzaKm','Katarina','Zivkovic',1),(8,'2018-06-06 12:32:18','kacaa','kacaa@gmail.com','$2y$10$UB/6Wn73FWmR9mT3rRdIJO9e1Erryj49l9VErKhgW8ZhERjxEa6ZG','Katarinaa','Zivkovicc',1),(9,'2018-07-01 08:32:29','snezana','snezana@gmail.com','$2y$10$qS6kSfc8Vh02wnGlHRDys.EUz50hW1kU.CBhDJYsq55eWR60sllLm','Snezana','Zivkovic',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'oglasi'
--

--
-- Dumping routines for database 'oglasi'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-05 12:00:14
