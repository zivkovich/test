<?php

//ako dodamo novu rutu moramo napraviti novu funkciju u model kontroleru (show,delete..)
return[
    App\Core\Route::get ('|^user/register/?$|',              'Main',        'getRegister'),
    App\Core\Route::post('|^user/register/?$|',              'Main',        'postRegister'),
    App\Core\Route::get ('|^user/login/?$|',                 'Main',        'getLogin'),
    App\Core\Route::post('|^user/login/?$|',                 'Main',        'postLogin'),
    App\Core\Route::get ('|^user/logout/?$|',                'Main',        'getLogout'),


    App\Core\Route::get ('|^admin/login/?$|',                'Main',        'getLogin'),
    App\Core\Route::post('|^admin/login/?$|',                'Main',        'postLogin'),

    App\Core\Route::get('|^model/([0-9]+)/?$|',              'Model',       'show'),
    App\Core\Route::get('|^model/([0-9]+)/delete/?$|',       'Model',       'delete'),

    App\Core\Route::get('|^category/([0-9]+)/?$|',           'Category',    'show'),
    App\Core\Route::get('|^category/([0-9]+)/delete/?$|',    'Category',    'delete'),

    //prvo ruta pa se pravi kontroler
    App\Core\Route::get('|^ad/([0-9]+)/?$|',                 'Ad',          'show'),
    App\Core\Route::post('|^search/?$|',                     'Ad',          'postSearch'),
    App\Core\Route::post('|^searchAdvanced/?$|',             'Ad',          'postAdvancedSearch'),




    //App\Core\Route::get('|^model/([0-9]+)/delete/?$|', 'Model', 'delete'),

    #API RUTE:
    App\Core\Route::get('|^api/ad/([0-9]+)/?$|',             'ApiAd',        'show'),
    App\Core\Route::get('|^api/bookmarks/?$|',               'ApiBookmark',  'getBookmarks'),
    App\Core\Route::get('|^api/bookmarks/add/([0-9]+)/?$|',  'ApiBookmark',  'addBookmark'),
    App\Core\Route::get('|^api/bookmarks/clear/?$|',         'ApiBookmark',  'clear'),

    #User Role routes:klip42
    App\Core\Route::get('|^user/profile/?$|',                    'UserDashboard',           'index'),

    App\Core\Route::get('|^admin/profile/?$|',                   'AdminDashboard',           'index'),

    #jednu tabelu u bazi prati minimum 5 
    #lista svih za model koji imamo tj.tabelu, edit za dve komponente get i post, i add za get i post
    App\Core\Route::get ('|^admin/categories/?$|',                 'AdminCategoryManagement',  'categories'),
    App\Core\Route::get ('|^admin/categories/edit/([0-9]+)/?$|',   'AdminCategoryManagement',  'getEdit'),
    App\Core\Route::post('|^admin/categories/edit/([0-9]+)/?$|',  'AdminCategoryManagement',  'postEdit'),
    App\Core\Route::get ('|^admin/categories/add/?$|',             'AdminCategoryManagement',  'getAdd'),
    App\Core\Route::post('|^admin/categories/add/?$|',            'AdminCategoryManagement',  'postAdd'),

    App\Core\Route::get ('|^admin/models/?$|',                     'AdminModelManagement',  'models'),
    App\Core\Route::get ('|^admin/models/edit/([0-9]+)/?$|',       'AdminModelManagement',  'getEdit'),
    App\Core\Route::post('|^admin/models/edit/([0-9]+)/?$|',      'AdminModelManagement',  'postEdit'),
    App\Core\Route::get ('|^admin/models/add/?$|',                 'AdminModelManagement',  'getAdd'),
    App\Core\Route::post('|^admin/models/add/?$|',                'AdminModelManagement',  'postAdd'),

    #klip 59 
    App\Core\Route::get ('|^user/ads/?$|',                         'UserAdManagement',  'ads'),
    App\Core\Route::get ('|^user/ads/edit/([0-9]+)/?$|',           'UserAdManagement',  'getEdit'),
    App\Core\Route::post('|^user/ads/edit/([0-9]+)/?$|',           'UserAdManagement',  'postEdit'),
    App\Core\Route::get ('|^user/ads/add/?$|',                     'UserAdManagement',  'getAdd'),
    App\Core\Route::post('|^user/ads/add/?$|',                     'UserAdManagement',  'postAdd'),





    App\Core\Route::any('|^.*$|',                            'Main',         'home')
];