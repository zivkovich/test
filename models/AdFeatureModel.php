<?php
    namespace App\Models;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class AdFeatureModel extends Model {
        protected function getFields(): array{
            return [
                'ad_feature_id'     =>  new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at'        =>  new Field((new DateTimeValidator())->allowDate()->allowTime() , false),

                'ad_id'             =>  new Field((new NumberValidator())->setIntegerLength(11)),
                'feature_id'        =>  new Field((new NumberValidator())->setIntegerLength(11)),
                'value'             =>  new Field((new StringValidator())->setMaxLength(255))

            ];
        }
     
        
    public function getAllAdFeaturesByAdId(int $adId): array {
            return $this->getAllByFieldName('ad_id', $adId);
    }


    public function deleteAllAdFeaturesByAdId(int $adId){
            $sql = 'DELETE FROM `ad_feature` WHERE `ad_id` = ?';

            $prep = $this->getConnection()->prepare($sql);
                if (!$prep){
                    echo('nije moguce izbrisati karakteristike!');
                return [];
                }
            return $prep->execute([ $adId ]);
            return $prep->fetchAll(\PDO::FETCH_OBJ);
    }


    public function getAllByFuelAndPrice(string $fuel, int $priceStarts, $priceEnds){
            $sql = 'SELECT * FROM ad 
            INNER JOIN ad_feature ON ad_feature.ad_id = ad.ad_id 
            WHERE  value = ? AND price BETWEEN ? AND ?;';

            $prep = $this->getConnection()->prepare($sql);
                if (!$prep){
                return [];
                }
            $res = $prep->execute([$fuel, $priceStarts, $priceEnds ]);
                if (!$res) {
                    return [];
                }
            return $prep->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getAllByBodyType(int $val, string $bodytype){
        $sql = 'SELECT * FROM ad_feature
        WHERE  ad_id = ? AND value = ? ;';


        $prep = $this->getConnection()->prepare($sql);
            if (!$prep){
            return [];
            }
        $res = $prep->execute([$val, $bodytype]);
            if (!$res) {
                return [];
            }
        return $prep->fetchAll(\PDO::FETCH_OBJ);
    }

         
}

    
  
    