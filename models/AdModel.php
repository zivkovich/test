<?php
    namespace App\Models;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;
    use App\Core\DatabaseConnection;

    class AdModel extends Model {
        protected function getFields(): array{
            return [
                'ad_id'      => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at' => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                'starts_at'  => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),
                'ends_at'    => new Field((new DateTimeValidator())->allowDate()->allowTime(), false),

                'title'           => new Field ((new StringValidator())->setMaxLength(255)) , 
                'description'     => new Field ((new StringValidator())->setMaxLength(64*1024)) ,
                'price'           => new Field ((new NumberValidator())->setInteger()
                                                                       ->setIntegerLength(11)),
                
                'is_active'       => new Field (new BitValidator()),
                'model_id'        => new Field ((new NumberValidator())->setIntegerLength(11)),
                'user_id'         => new Field ((new NumberValidator())->setIntegerLength(11)),
                'image_path'      => new Field ((new StringValidator())->setMaxLength(255)) , 


            ];
        }
     
        public function getAllByModelId(int $modelId): array {
            return $this->getAllByFieldName('model_id', $modelId);
         }
//klip 59 
        public function getAllByUserId(int $userId): array {
            return $this->getAllByFieldName('user_id', $userId);
         }

//52
         public function getAllBySearch(string $keywords){
             $sql = 'SELECT * FROM `ad` WHERE `title` LIKE ? OR `description` LIKE ?;';

             $keywords = '%' . $keywords . '%';

             $prep = $this->getConnection()->prepare($sql);
             if (!$prep){
                return [];
             }

             $res = $prep->execute([$keywords, $keywords]);
             if (!$res) {
                 return [];
             }
             return $prep->fetchAll(\PDO::FETCH_OBJ);

         }

         /*OVAJ METOD JE I U ADFEATURE MODELU PORED CENE MOZE SE IZABRATI I GORIVO
         public function getAllByPrice(int $priceStarts, $priceEnds){
            $sql = 'SELECT * FROM `ad` WHERE `price` BETWEEN ? AND ?;';

            $prep = $this->getConnection()->prepare($sql);
            if (!$prep){
               return [];
            }

            $res = $prep->execute([$priceStarts, $priceEnds]);
            if (!$res) {
                return [];
            }
            return $prep->fetchAll(\PDO::FETCH_OBJ);

        }
        */



      

    

      
        



      }