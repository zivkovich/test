<?php
    namespace App\Models;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;


    class ModelModel extends Model {
        protected function getFields(): array{
            return [
                'model_id' => new Field((new NumberValidator())->setIntegerLength(11), false),
              
                'name'        => new Field((new StringValidator())->setMaxLength(255)) , 
                'category_id' => new Field((new NumberValidator())->setIntegerLength(11))
           
             ];
        }
       

        public function getAllByCategoryId(int $categoryId): array {
            return $this->getAllByFieldName('category_id', $categoryId);
            
         }

         public function getAllByModelId(int $modelId): array {
            return $this->getAllByFieldName('model_id', $modelId);
            
         }

         
         

    
 

    }