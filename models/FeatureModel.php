<?php
    namespace App\Models;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    
    class FeatureModel extends Model {
        protected function getFields(): array{
            return [
                'feature_id' => new Field((new NumberValidator())->setIntegerLength(11), false),
                'created_at' => new Field((new DateTimeValidator())->allowDate()->allowTime() , false),

                'name'           => new Field ((new StringValidator())->setMaxLength(255)) , 
                'is_mandatory'   => new Field(new BitValidator()),

            ];
        }
    
        public function getAllByFeatureId(int $featureId): array {
           return $this->getAllByFieldName('feature_id', $featureId);
        }

        
    }

    